import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableNativeFeedback
} from 'react-native';

const CategoryGridTitle = (props) => {
    let TouchableCmp = TouchableOpacity;
    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableCmp = TouchableNativeFeedback;
    }
    return (
        <View style={styles.gridItem} >
            <TouchableCmp
                style={styles.touchable}
                onPress={props.onSelect}
            >
                <View style={{ ...styles.container, ...{ backgroundColor: props.color } }} >
                    <Text numberOfLines={2} style={styles.title}>{props.title}</Text>
                </View>
            </TouchableCmp>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        padding: 15,
        justifyContent: "flex-end",
        alignItems: "flex-end",
    },
    gridItem: {
        flex: 1,
        margin: 15,
        height: 150,
        borderRadius: 10,
        overflow: Platform.OS === 'android' && Platform.Version >= 21
            ? 'hidden'
            : 'visible',
        elevation: 5,
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 21,
        textAlign: "right",
    },
    touchable: {
        flex: 1,
    }
});

export default CategoryGridTitle;